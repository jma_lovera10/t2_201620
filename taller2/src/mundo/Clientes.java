package mundo;

import java.util.Date;

public class Clientes extends Trabajo{

	public enum TipoNegocio{
		PRODUCCION,
		SERVICIOS,
		EXTRACCION,
		VENTAS,
		OTRO_TIPO_DE_NEGOCIO;
	}
	
	protected TipoNegocio tipoNegocio;
	
	public Clientes(Date pFecha, TipoEventoTrabajo pTipo,String pLugar, boolean pObligatorio, boolean pFormal, TipoNegocio pTipoNegocio) {
		super(pFecha,pTipo, pLugar, pObligatorio, pFormal);
		tipoNegocio = pTipoNegocio;
	}
	
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString()
	{
	  return "Evento con CLIENTES: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nTIPO EVENTO: " + this.tipoEventoTrabajo
	  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
	  		+"\nFORMAL: "+ conv(this.formal);
	}
}
