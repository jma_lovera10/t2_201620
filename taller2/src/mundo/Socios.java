package mundo;

import java.util.Date;

public class Socios extends Trabajo{

	public enum Empresa{
		INTEL,
		NI,
		IBM,
		APPLE,
		MICROSOFT,
		GOOGLE,
		OTRA_EMPRESA;
		
	}
	
	protected Empresa empresa;
	
	public Socios(Date pFecha, TipoEventoTrabajo pTipoEventoTrabajo,String pLugar, boolean pObligatorio, boolean pFormal, Empresa pEmpresa) {
		super(pFecha,pTipoEventoTrabajo ,pLugar, pObligatorio, pFormal);
		empresa = pEmpresa;
	}
	
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString()
	{
	  return "Evento con SOCIOS: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nTIPO EVENTO: " + this.tipoEventoTrabajo
	  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
	  		+"\nFORMAL: "+ conv(this.formal);
	}

}
