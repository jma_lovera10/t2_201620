package mundo;

import java.util.Date;

public class Trabajo extends Evento{

	public enum  TipoEventoTrabajo {
		JUNTA,
		ALMUERZO,
		CENA,
		COCTEL,
		PRESENTACION,
		OTRA_ACTIVIDAD_DE_TRABAJO,
	}
	
	protected TipoEventoTrabajo tipoEventoTrabajo;
	
	public Trabajo(Date pFecha, TipoEventoTrabajo pTipo,String pLugar, boolean pObligatorio, boolean pFormal) {
		super(pFecha, pLugar, pObligatorio, pFormal);
		// TODO Auto-generated constructor stub
		tipoEventoTrabajo = pTipo;
	}

}
